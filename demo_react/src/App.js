import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import horseShoeChart, { createTextOption } from '@shjeon0730/horseshoe-chart';

import './App.css';
import {
  createLinkObj,
  createTextObj,
} from '@shjeon0730/svg-gen-utils/src/txtUtils';

import { createElementInfo } from '@shjeon0730/svg-gen-utils/src/renderUtils';

const defaultAttr = {
  config: {
    outRadius: 50,
    inRadius: 40,
    numOfUnits: 8,
    levelBorderWidth: 2,
    filled: 5,
    centeGap: 60,
    offsetX: 0,
    offsetY: 0,
    rotate: 0,
    selColor: [{ boundary: [0, 1], color: 'red' }],
    backColor: [
      { maxIdx: 1, color: '#fff0f0' },
      { maxIdx: 5, color: '#fffeef' },
      { maxIdx: 6, color: '#efffef' },
    ],
  },
  centerTexts: ['color area setted chart'],
  id: 'default',
};

const oneColor = {
  config: {
    levelBorderWidth: 0.1,
    numOfUnits: 40,
    filled: 4,
    selColor: '#12395b',
    backColor: '#efefef',
  },
  centerTexts: ['uni color chart'],
  id: 'one-color',
};

const multiLineText = {
  config: {
    levelBorderWidth: 0.1,
    numOfUnits: 10,
    filled: 0,
    selColor: '#12395b',
    backColor: '#efefef',
  },
  centerTexts: {
    defaultOptions: createTextOption(9, 1, 'darkgreen'),
    items: ['multi line', 'text with', 'defaultOptions'],
  },
  id: 'multiline-text',
};

const almostCircle = {
  config: {
    centerGap: 1,
    levelBorderWidth: 1,
    numOfUnits: 10,
    filled: 8,
    roundEdgeSize: 0,
  },
  centerTexts: ['almost circle'],
  id: 'almost-circle',
};

const partialFilled = {
  config: {
    centerGap: 60,
    levelBorderWidth: 1,
    numOfUnits: 4,
    filled: 2.5,
    roundEdgeSize: 1,
  },
  centerTexts: ['partial filled'],
  id: 'partial-filled',
};

const flatEdge = {
  config: {
    numOfUnits: 10,
    filled: 4,
    roundEdgeSize: 0,
  },
  centerTexts: ['flat edge graph'],
  id: 'flat-edge',
};

const rotated = {
  config: {
    rotate: 40,
  },
  centerTexts: ['40 degree', 'rotated', 'chart'],
  id: 'rotated',
};

const withLink = {
  config: {
    rotate: 40,
  },
  centerTexts: [
    'text1',
    createLinkObj(
      'link',
      9,
      1,
      'blue',
      'http://www.google.com',
      {
        style: { textDecoration: 'none' },
        target: '_blank',
      },
      { className: 'link-style' }
    ),
    'linked',
    'chart',
  ],
  id: 'with-link',
};

const arc180WithSvg = {
  config: {
    outRadius: 35,
    inRadius: 30,
    numOfUnits: 3,
    levelBorderWidth: 2,
    roundEdgeSize: 0,
    filled: 0,
    centerGap: 180,
    offsetX: 0,
    offsetY: 0,
    rotate: 0,
    selColor: 'white',
    backColor: ['red', 'yellow', 'green'],
  },
  centerTexts: ['this is arc', '180', '', ''],
  id: 'arc-180-with-svg',
  style: {
    height: '120px',
    overflow: 'hidden',
  },
  viewBox: '-45 -45 90 90',
  children: [
    createElementInfo(
      'text',
      {
        x: '-35',
        y: '6',
        fill: 'red',
        textAnchor: 'middle',
        fontSize: '7',
      },
      ['left']
    ),
    createElementInfo(
      'text',
      {
        x: '35',
        y: '6',
        fill: 'blue',
        textAnchor: 'middle',
        fontSize: '7',
      },
      ['right']
    ),
    createElementInfo('g', { id: 'top-needle', transform: 'rotate(0 0 0)' }, [
      createElementInfo('path', {
        d: 'M 5,-25 L-5,-25 L0,-35 z',
        fill: 'black',
        stroke: 'gray',
        strokeWidth: '1',
      }),
    ]),
  ],
};

const arc180WithSvg2 = {
  config: {
    outRadius: 40,
    inRadius: 30,
    numOfUnits: 3,
    levelBorderWidth: 2,
    roundEdgeSize: 0,
    filled: 0,
    centerGap: 180,
    offsetX: 0,
    offsetY: 0,
    rotate: 0,
    selColor: 'white',
    backColor: ['red', 'yellow', 'green'],
  },
  centerTexts: [
    'this is arc',
    '180',
    '',
    '',
    createTextObj('left', 7, 0, 'blue', { dx: '-35', dy: '-15' }),
    createTextObj('right', 7, -7, 'red', { dx: '35', dy: '-15' }),
  ],
  id: 'arc-180-with-svg1',
  style: {
    height: '120px',
    overflow: 'hidden',
  },
  children: [
    createElementInfo('g', { id: 'top-needle', transform: 'rotate(0 0 0)' }, [
      createElementInfo('path', {
        d: 'M 5,-25 L-5,-25 L0,-35 z',
        fill: 'black',
        stroke: 'gray',
        strokeWidth: '1',
      }),
    ]),
  ],
  viewBox: '-45 -45 90 90',
};

const withLinkTextInOneLine = {
  config: {
    rotate: 40,
  },
  centerTexts: [
    'with',
    createLinkObj(
      'Link',
      9,
      -9,
      'blue',
      'http://www.google.com',
      {
        style: { textDecoration: 'none' },
        target: '_blank',
      },
      { dy: '10', dx: '-10', fontSize: '9', className: 'link-style' }
    ),
    createTextObj('text', 9, 1, 'red', { dx: '10' }),
    'in',
    'one line',
  ],
  id: 'with-link-text-in-one-line',
};

const withNeedle = {
  config: {
    numOfUnits: 10,
    filled: 4,
    roundEdgeSize: 0,
  },
  centerTexts: [' ', 'with needle'],
  needles: [
    {
      headRadius: 3,
      needleLength: 45,
      id: 'needle1',
      rotate: 0,
      strokeColor: 'transparent',
      fillColor: 'red',
      strokeWidth: 0.1,
    },
    {
      id: 'needle2',
      headRadius: 2,
      rotate: 30,
      fillColor: 'blue',
      needleLength: 30,
    },
  ],
  id: 'with-needle',
};

let angle = 0;
let timerId;

let flatEdgeGraph;
function flatEdgeMotion() {
  flatEdgeGraph =
    flatEdgeGraph || document.querySelector('#flat-edge>#horseShoe');
  flatEdgeGraph &&
    flatEdgeGraph.setAttribute('transform', `rotate(${angle * 10} 0 0)`);
}

let needle1, needle2;
function withNeedleMotion() {
  const speed = 4;
  needle1 = needle1 || document.querySelector('#with-needle>#needle1');
  needle2 = needle2 || document.querySelector('#with-needle>#needle2');
  needle2.setAttribute('transform', `rotate(${angle / 36.0 * 5 * speed} 0 0)`);
  needle1.setAttribute('transform', `rotate(${angle / 36.0 * 60 * speed} 0 0)`);
}

let uniColorMap = new Map();
function uniColorChartMotion() {
  const fillup = angle % 40;
  let fillCell;
  if (fillup === 0) {
    for (let i = 1; i < 40; i++) {
      fillCell = uniColorMap[i];
      if (!fillCell) {
        fillCell = document.querySelector('#one-color>#horseShoe>#level-' + i);
      }
      fillCell.setAttribute('fill', '#efefef');
    }
    fillCell = uniColorMap[0];
    if (!fillCell) {
      fillCell = document.querySelector('#one-color>#horseShoe>#level-0');
    }
    fillCell.setAttribute('fill', '#12395b');
  } else {
    fillCell = uniColorMap[fillup];
    if (!fillCell) {
      fillCell = document.querySelector(
        '#one-color>#horseShoe>#level-' + fillup
      );
    }
    fillCell.setAttribute('fill', '#12395b');
  }
}

let halfArch1;
let halfArch2;
function guageMotions() {
  const guageAngle = (Math.floor(angle / 10) % 3 - 1) * 60;
  halfArch1 =
    halfArch1 || document.querySelector('#arc-180-with-svg>#top-needle');
  halfArch1 && halfArch1.setAttribute('transform', `rotate(${guageAngle} 0 0)`);
  halfArch2 =
    halfArch2 || document.querySelector('#arc-180-with-svg1>#top-needle');
  halfArch2 && halfArch2.setAttribute('transform', `rotate(${guageAngle} 0 0)`);
}

function HorseShoe(props) {
  return horseShoeChart(React.createElement, props);
}

function App() {
  const attrLists = [
    defaultAttr,
    oneColor,
    multiLineText,
    almostCircle,
    flatEdge,
    rotated,
    withLink,
    withLinkTextInOneLine,
    withNeedle,
    partialFilled,
    arc180WithSvg,
    arc180WithSvg2,
  ];
  const [timerOn, setTimerOn] = useState('ON');
  const onClick = (e) => {
    if (timerId === undefined) {
      const id = setInterval(() => {
        flatEdgeMotion();
        withNeedleMotion();
        uniColorChartMotion();
        guageMotions();
        angle = angle + 1;
      }, 100);
      timerId = id;
      setTimerOn('OFF');
    } else {
      clearInterval(timerId);
      timerId = undefined;
      setTimerOn('ON');
    }
  };
  // useEffect(() => {

  // }, [timerOn]);

  return (
    <div style={{ display: 'inline' }}>
      <div style={{ width: '200px', height: '50px', display: 'block' }}>
        <button onClick={onClick} style={{ width: '100%', height: '100%' }}>
          {'Timer ' + timerOn}
        </button>
      </div>

      {attrLists.map((conf, idx) => {
        const { children, style, ...rest } = conf;
        return (
          <div
            key={`chart-${idx}`}
            style={{
              display: 'inline-block',
              width: '200px',
              height: '200px',
              ...style,
            }}
          >
            {<HorseShoe {...rest}>{children || []}</HorseShoe>}
          </div>
        );
      })}
    </div>
  );
}

export default App;
