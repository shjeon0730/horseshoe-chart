### Horseshoe chart

high chart component written in commonjs. 
it did not use specific framework( even though it is tested in React.js), so 
You can use this component with your framework since this chart just returns
javascript objects and functions.

### suported functionalities

- Highchart with various angle range.
- round/flat shape edge. also you can set the radius of the round edge
- create a group of paths in the svg
- center text (color, fontSize, multiline support)
- link with accessiblility support
- area coloring support
- unit devided fill (ex> fill 3 cells out of 10)
- rotate chart support
- offset x,y support
- multiple needle supports (ex> clock)
- partial fill support

### demo link : 
[demo mp4](https://gitlab.com/shjeon0730/horseshoe-chart/-/raw/master/demo.mp4)
![demo gif](https://gitlab.com/shjeon0730/horseshoe-chart/-/raw/master/demo.gif)
