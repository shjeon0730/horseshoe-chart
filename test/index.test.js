const { createTextOption,
  createTextItem,
  createLinkItem,
  default: chart } = require('../src');
const mathCopy = Object.create(global.Math);

describe('main', () => {
  let mathOrg;

  beforeAll(() => {
    mockMath = Object.create(global.Math);
    mockMath.random = () => 0.1;//random will always return 0.1
    global.Math = mockMath;
  });
  afterAll(() => {
    global.Math = mathCopy;
  });
  it('default should be a function', () => {
    expect(typeof chart).toBe('function')
  });

  it.only('chart should return object', () => {

    const defaultAttr = {
      config: {
        outRadius: 50,
        inRadius: 40,
        numOfUnits: 2,
        levelBorderWidth: 2,
        filled: 1,
        centeGap: 60,
        offsetX: 0,
        offsetY: 0,
        rotate: 0,
        selColor: [{ boundary: [0, 1], color: 'red' }],
        backColor: [
          { maxIdx: 1, color: '#fff0f0' },
          { maxIdx: 5, color: '#fffeef' },
          { maxIdx: 6, color: '#efffef' }
        ]
      },
      centerTexts: ['color area setted chart']
    };
    const expectedChart = {
      "tagName": "svg",
      "attr": { "key": "ele-0", "id": "horseShoe10000", "viewBox": "-51 -51 102 102", "aria-labelledby": "title-id desc-id", "xmlns": "http://www.w3.org/2000/svg" },
      "children": [
        { "tagName": "title", "attr": { "id": "title-id" }, "children": "svg shoechart" },
        { "tagName": "desc", "attr": { "id": "desc-id" }, "children": "svg shoehorse" },
        {
          "tagName": "g",
          "attr": { "key": "ele-2", "id": "horseShoe", "transform": "rotate(0 0 0)" },
          "children": [
            { "tagName": "path", "attr": { "key": "level-0", "d": "m -19.999999999999993,34.64101615137755 a5,5 0 0,1 -4.9999999999999964,8.660254037844389 a50,50 0 0,1 24.127379678135814,-93.2936549470415 l0.17452406437283507,9.998476951563916 a40,40 0 0,0 -19.301903742508653,74.63492395763319 z", "fill": "red", "stroke": "black", "strokeWidth": "0.1" }, "children": "" },
            { "tagName": "path", "attr": { "key": "level-1", "d": "m 0.8726203218641565,-49.992384757819565 a50,50 0 0,1 24.127379678135828,93.2936549470415 a5,5 0 0,1 -4.9999999999999964,-8.660254037844382 a40,40 0 0,0 -19.301903742508664,-74.6349239576332 l0.1745240643728313,-9.998476951563916 z", "fill": "#fff0f0", "stroke": "gray", "strokeWidth": "0.1" }, "children": "" }
          ]
        },
        {
          "tagName": "g",
          "attr": { "key": "ele-3", "id": "centerText", "focusable": "true" },
          "children": [
            { "tagName": "text", "attr": { "key": "text-0", "x": 0, "y": 4.5, "fill": "black", "textAnchor": "middle", "fontSize": 8 }, "children": "color area setted chart" }
          ]
        }
      ]
    }
    const result = chart(renderFunc, defaultAttr);

    expect(result).toEqual(expectedChart);
    expect(expectedChart.children.find(i => i.tagName === 'desc')).not.toBeNull();
    expect(expectedChart.children.find(i => i.tagName === 'title')).not.toBeNull();
    const groups = expectedChart.children.filter(i => i.tagName === 'g');
    expect(groups.length).toBe(2);
    const chartItems = groups.find(g => g.attr.id === 'horseShoe');
    const textItems = groups.find(g => g.attr.id === 'centerText');
    expect(chartItems).not.toBeNull();

    expect(chartItems.children[0].tagName).toBe('path');
    expect(chartItems.children[0].attr.fill).toBe('red');
    expect(chartItems.children[1].tagName).toBe('path');
    expect(chartItems.children[1].attr.fill).toBe('#fff0f0');

    expect(textItems).not.toBeNull();
    const textItem0 = textItems.children[0];
    expect(textItem0.tagName).toBe('text');
    expect(textItem0.attr.fill).toBe('black');
    expect(textItem0.attr.fontSize).toBe(8);
    expect(textItem0.children).toBe('color area setted chart');
  });
});

function renderFunc(tagName, attr, children = []) {
  return {
    tagName,
    attr,
    children
  };
}