/*******************************************************************************
 * Copyright (C) 2020
 * Songhyeon Jun(shjeon0730@gmail.com)
 * All Rights Reserved
 *
 * File: HorseShoeChart.js
 *******************************************************************************/
const svgUtil = require('@shjeon0730/svg-gen-utils');
const { drawHorseShoe, drawCenterText, drawNeedle } = require('./drawers');

const createElementInfo = (...args) => {
  return svgUtil.renderer.createElementInfo(...args);
};

const createTextOption = svgUtil.text.createDefaultOption;
const createTextItem = svgUtil.text.createTextObj;
const createLinkItem = svgUtil.text.createLinkObj;

function HorseShoeChart(props = { children: [] }) {
  let { config = {}, centerTexts, needles = [], children, ...rest } = props;
  config = {
    outRadius: 50,
    inRadius: 40,
    numOfUnits: 8,
    levelBorderWidth: 2,
    filled: 2,
    centerGap: 60,
    offsetX: 0,
    offsetY: 0,
    rotate: 0,
    selColor: [{ boundary: [0, 7], color: '#12395b' }],
    backColor: [
      { maxIdx: 1, color: '#fff0f0' },
      { maxIdx: 5, color: '#fffeef' },
      { maxIdx: 7, color: '#efffef' },
    ],
    ...config,
  };

  const viewBoxWidth = config.outRadius * 2 + 2; //2 is right/left margin
  const viewBoxHeight = config.outRadius * 2 + 2; //2 is top/bottom margin
  const id = props.id || 'horseShoe' + Math.round(Math.random() * 100000);
  const descId = 'desc-id';
  const titleId = 'title-id';
  const left = -config.outRadius - 1; //-1 is left margin
  const top = -config.outRadius - 1; // -1 is top margin
  return createElementInfo(
    'svg',
    {
      id,
      viewBox: `${left} ${top} ${viewBoxWidth} ${viewBoxHeight}`,
      'aria-labelledby': [titleId, descId].join(' '),
      xmlns: 'http://www.w3.org/2000/svg',
      ...rest,
    },
    [
      createElementInfo(
        'title',
        { id: titleId, key: 'title' },
        'svg shoechart'
      ),
      createElementInfo('desc', { id: descId, key: 'desc' }, 'svg shoehorse'),
      drawHorseShoe(config),
      drawCenterText(centerTexts, config.offsetX, config.offsetY),
      ...needles.map((needle) => drawNeedle(needle)),
      ...children,
    ]
  );
}

function horseShoeChartRender(renderFunction, attributes) {
  return svgUtil.renderer.render(renderFunction, HorseShoeChart(attributes));
}

module.exports = {
  __esModule: { value: true },
  createTextOption,
  createTextItem,
  createLinkItem,
  default: horseShoeChartRender,
};
