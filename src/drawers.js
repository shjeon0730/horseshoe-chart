const svgUtil = require('@shjeon0730/svg-gen-utils');
const { drawChart, getPt } = require('./utils');

const createElementInfo = (...args) => {
  return svgUtil.renderer.createElementInfo(...args);
};

const drawNeedle = (needleConfig = {}) => {
  const {
    headRadius = 5,
    needleLength = 50,
    id = 'needle',
    rotate = 0,
    strokeColor = 'transparent',
    fillColor = 'red',
    strokeWidth = 0.1,
    ...restOrg
  } = needleConfig;
  const rest = restOrg || {};
  const path = svgUtil.path.pathUtils(getPt(headRadius, 0));

  const builder = path.element;
  const elements = [
    builder.arc(getPt(-headRadius, 0), headRadius, 0, 1),
    builder.line(getPt(0, -needleLength)),
  ];

  return createElementInfo(
    'g',
    {
      id,
      transform: `rotate(${rotate} 0 0)`,
    },
    [path.render(elements, strokeColor, fillColor, strokeWidth, rest)]
  );
};

const drawHorseShoe = (config = {}) => {
  const rotate = config.rotate || 0;
  const transform = 'rotate(' + rotate + ' 0 0)';

  const chart = drawChart(config);
  return createElementInfo('g', { id: 'horseShoe', transform }, chart);
};

const drawCenterText = (centerTexts, offsetX = 0, offsetY = 0) => {
  if (centerTexts) {
    const txtUtil = svgUtil.text;
    let txtElement;
    if (centerTexts.length) {
      txtElement = txtUtil.drawCenterTexts(centerTexts, {
        x: offsetX,
        y: offsetY,
      });
    } else if (centerTexts.items && centerTexts.defaultOptions) {
      txtElement = txtUtil.drawCenterTexts(
        centerTexts.items,
        { x: offsetX, y: offsetY },
        centerTexts.defaultOptions
      );
    } else {
      throw Error(
        'centerTexts must be [textItem1, ...] or {items:[string,...], defaultOptions:{...}}. \n received:' +
          JSON.stringify(centerTexts)
      );
    }
    return createElementInfo(
      'g',
      { id: 'centerText', focusable: 'true' },
      txtElement
    );
  } else {
    return '';
  }
};

module.exports = {
  drawHorseShoe,
  drawCenterText,
  drawNeedle,
};
